# Informática

[1º, Grado en Física][fis]<br>
[Universidad de Murcia][um]

Profesor: [Alberto Ruiz García][yo] (parte I)

[Calendario](https://www.um.es/documents/14152/10096322/Primero-C1+curso2018-19.pdf)

[Apuntes](notebooks/index.ipynb)

[yo]: http://dis.um.es/profesores/alberto
[fis]: http://www.um.es/web/quimica/contenido/estudios/grados/fisica
[um]: http://dis.um.es/


